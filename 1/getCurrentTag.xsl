<?xml version="1.0" encoding="utf-8"?>

<!--
public function getCurrentTag(); // Получить последнюю по дате версию
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
	<xsl:template match="/changelog">
		<xsl:for-each select="release">
			<xsl:sort select="@date" order="descending"/>
			<xsl:apply-templates select=".">
				<xsl:with-param name="position" select="position()"/>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="release">
		<xsl:param name="position"/>
		<xsl:value-of select="update[$position=1][last()]"/>
	</xsl:template>
</xsl:stylesheet>