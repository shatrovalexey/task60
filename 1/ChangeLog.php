<?php
interface ChangelogInterface {
	public function getCurrentTag( ) ; // Получить последнюю по дате версию
	public function getChangelog( ) ; // Получить многомерный ассоц. массив для передачи в шаблонизатор, при этом изменив формат даты на d.m.Y
}

class ChangelogClass implements ChangelogInterface {
	/**
	* @var \DOMXPath $xpathh - объект для выполнения запросов XPath по документу XML
	*/
	protected $xpathh ;

	/**
	* Конструктор
	*
	* @param string $file_name - имя файла XML
	*/
	public function __construct( $file_name ) {
		/**
		* @var \DOMDocument $domh
		*/
		$domh = new \DOMDocument( ) ;

		if ( ! $domh->load( $file_name ) ) {
			throw new \Exception( 'Can\'t load ' . $file_name . ' as XML' ) ;
		}

		$this->xpathh = new \DOMXPath( $domh ) ;
	}

	/**
	* Получить последнюю по дате версию
	*
	* @return string - дата и время
	*/
	public function getCurrentTag( ) {
		/**
		* @var string $result - результат
		* @var string $last_date - максимальная дата
		*/
		$result = $last_date = null ;

		foreach ( $this->xpathh->query( '/changelog/release' ) as $release ) {
			$current_date = $release->getAttribute( 'date' ) ;

			if ( ! is_null( $last_date ) && ( $last_date >= $current_date ) ) {
				continue ;
			}

			try {
				$result = $this->xpathh->query( 'update[last( )]/text( )' , $release )->item( 0 )->textContent ;
			} catch ( \Exception $exception ) {
				continue ;
			}

			$last_date = $current_date ;
		}

		return $result ;
	}

	/**
	* Получить многомерный ассоц. массив для передачи в шаблонизатор, при этом изменив формат даты на d.m.Y
	*
	* @return array - многомерный ассоц. массив для передачи в шаблонизатор, при этом изменив формат даты на d.m.Y
	*/
	public function getChangelog() {
		/**
		* @var mixed $result - результат
		*/
		$result = array( ) ;

		foreach ( $this->xpathh->query( '/changelog/release' ) as $release ) {
			$item = array(
				'attr' => array(
					'tag' => $release->getAttribute( 'tag' ) ,
					'date' => $this->_dateFormat( $release->getAttribute( 'date' ) )
				) ,
				'update' => array( )
			) ;

			foreach ( $this->xpathh->query( 'update/text( )' , $release ) as $update ) {
				$item[ 'update' ][] = $update->textContent ;
			}

			$result[] = $item ;
		}

		return $result ;
	}

	/**
	* Форматирование даты из YYYY-mm-dd в dd.mm.YYYY
	*
	* @param string $date - дата в формате YYYY-mm-dd
	*
	* @return string - дата в формате dd.mm.YYYY
	*/
	protected function _dateFormat( $date ) {
		return strftime( '%d.%m.%Y' , strtotime( $date ) ) ;
	}
}