<?xml version="1.0" encoding="utf-8"?>

<!--
public function getChangelog(); // Получить многомерный ассоц. массив для передачи в шаблонизатор, при этом изменив формат даты на d.m.Y
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:regexp="http://exslt.org/regular-expressions"
>
	<xsl:template match="/changelog">
		[<xsl:apply-templates/>]
	</xsl:template>

	<xsl:template match="release">{
			"tag" : "<xsl:value-of select="@tag"/>" ,
			"date" : "<xsl:call-template name="date-format"/>" ,
			"features" : [<xsl:apply-templates select="update"/>]
		}
		<xsl:if test="position()!=last()">,</xsl:if>
	</xsl:template>

	<xsl:template match="update">
		"<xsl:call-template name="json-format"/>"
		<xsl:if test="position()&lt;last()">,</xsl:if>
	</xsl:template>

	<xsl:template name="date-format">
		<xsl:value-of select="regexp:replace( @date , '(\d{4})\-(\d{1,2})\-(\d{1,2})' , 'g' , '$3.$2.$1' )"/>
	</xsl:template>

	<xsl:template name="json-format">
		<xsl:value-of select="regexp:replace( regexp:replace( regexp:replace( text() , '([^\w\s])' , 'g' , '\$1' ) , '(\r)' , 'g' , '\r' ) , '(\n)' , 'g' , '\n' )"/>
	</xsl:template>
</xsl:stylesheet>