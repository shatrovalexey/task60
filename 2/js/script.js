jQuery.fn.loadTable = function( ) {
	let $self = jQuery( this ).on( "click" , function( ) {
		jQuery.ajax( {
			"url" : $self.data( "src" ) ,
			"dataType" : "json" ,
			"success" : function( $data ) {
				let $modal = jQuery( $self.data( "target" ) ) ;
				let $tbody = $modal.find( ".modal-body .table.table-hover tbody" ) ;
				
				$tbody.find( "tr:not(.nod)" ).remove( ) ;

				let $tr = $tbody.find( "tr.nod" ) ;

				jQuery( $data.items ).each( function( ) {
					let $tr_current = $tr.clone( true ).removeClass( "nod" ) ;

					for ( let $key in this ) {
						$tr_current.find( "[data-key=" + $key + "]" ).text( this[ $key ] ) ;
					}

					$tbody.append( $tr_current ) ;
				} ) ;

				$modal.modal( "show" ) ;
			}
		} ) ;
	} ) ;
} ;

jQuery( function( ) {
	jQuery( ".btn.btn-info.btn-lg" ).loadTable( ) ;
} ) ;